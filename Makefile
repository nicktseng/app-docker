DC=docker-compose
MC=server
FT=client

ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    detected_OS := Windows
else
    detected_OS := $(shell uname)  # same as "uname -s"
endif

init:
	@echo "INIT PROJECT"
	@echo "Copying .env.dist in .env"
	@cp ./server/.env.dist ./server/.env
	@cp ./client/.env.dist ./client/.env.development
	@cp ./client/.env.dist ./client/.env.production
	@if [ ${detected_OS} = "Darwin" ]; then\
		export LC_CTYPE=C;\
    	sed -i "" "s@SECRET_TO_CHANGE@`tr -dc A-Za-z0-9_ < /dev/urandom | head -c 64 | xargs`@g" ./server/.env;\
	elif [ ${detected_OS} = "Linux" ]; then\
    	sed -i "s@SECRET_TO_CHANGE@`tr -dc A-Za-z0-9_ < /dev/urandom | head -c 64 | xargs`@g" ./server/.env;\
	else\
		echo "WINDOWS SYSTEM PLEASE COPY .env.dist TO .env AND SET VARIABLE";\
	fi
	@echo "\n.env:"
	@cat ./server/.env
	@echo "\n"

start: ## Build and launch the project in background
	@echo "Launch dettached projet and build\n"
	$(DC) up -d --build

startd: ## Build and launch the project in background
	@echo "Launch dettached projet and build\n"
	$(DC) -f docker-compose.dev.yml up -d --build

clean: ## Stop and delete the project stack
	$(DC) -f docker-compose.yml down
	$(DC) down --rmi all

cleand: ## Stop and delete the project stack
	$(DC) -f docker-compose.dev.yml down
	$(DC) -f docker-compose.dev.yml down --rmi all

logs: ## Attach to standard output of containers (to see logs)
	$(DC) logs -f $(MC)

logsd: ## Attach to standard output of containers (to see logs)
	$(DC) -f docker-compose.dev.yml logs -f $(MC)

front:
	$(DC) logs -f $(FT)

frontd:
	$(DC) -f docker-compose.dev.yml logs -f $(FT)

re: clean start

red: cleand startd

exec: ## Execute command inside api container
	$(DC) exec $(MC) bash

execd: ## Execute command inside api container
	$(DC) -f docker-compose.dev.yml exec $(MC) bash

migrate:
	$(DC) exec $(MC) ./node_modules/knex/bin/cli.js migrate:make $(name)
