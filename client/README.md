# React + TypeScript + Vite

## Docker Build & Run

```bash
# build image tag v1.0.0
docker build -t client:v1.0.0 .

# 指定 docker file
docker build -t client:v1.0.0 -f ./Deploy.Dockerfile .
```

```bash
# container name client_v1.0.0, image client:v1.0.0
docker run --name client_v1.0.0 -p 8080:80 -d client:v1.0.0
```
