
// require('dotenv').config();
const           config                          = require ('config');
const           http                            = require ('http');
const           service                         = require ('./app');
const         { logger }                        = require ('./util/logger');



let     configVal = {
        client:         undefined,
};

async function start (service) {
        let     server          = http.createServer (service).listen(config.app.port || 3000, config.app.host, () => {
                logger.info ('--------------------------------------------');
                logger.info (`App: \t${config.app.title}`);
                logger.info (`Server: \t${config.app.host || '127.0.0.1'}:${config.app.port}`);
                logger.info (`Env: \t${process.env.NODE_ENV || 'development'}`);
                logger.info (`Pid: \t${process.pid}`);
                logger.info ('--------------------------------------------');


        });
        
        configVal.client        = server;

        process.on ('SIGINT', closeServer);
        process.on ('SIGQUIT', closeServer);
        process.on ('SIGTERM', closeServer);
}

async function closeServer (signal) {
        logger.info (`Received ${signal}. Close my server properly.`);
        logger.debug (`Received ${signal}. Close my server properly.`);
        configVal.client.close (async function () {
                process.exit (0);
        });
}

start (service);
