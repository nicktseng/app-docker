const           models                          = require ('../model');
const         { logger }                        = require ('../util/logger');



async function read_apiA (req, res, next) {
        try {
                logger.info ('- /api/address/read read_apiA ()');
        
                let   { mobile }        = req.body;
        
                let     resjosn         = {
                        status: 100,
                        item:   'Hello Express'
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


async function create_apiA (req, res, next) {
        try {
                
                logger.info ('- /api/address/create create_apiA ()');
        
                let   { phonenum,
                        username,
                        password,
                        nickname,
                        name,
                        sex,
                        avatar,
                        title,
                        position,
                        province,
                        city,
                        district,
                        street,
                        role }          = req.body;
        
                        
                let   { User }          = models;
                
                let     user_ro =
                await User.create (req.body, { raw: true });
        
                console.log (JSON.stringify (user_ro, null, 4));
        
                let     resjosn         = {
                        status: 100,
                        item:   'Hello Express'
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


module.exports = {
        read_apiA,
        create_apiA,
};
