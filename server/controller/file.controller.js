const           models                          = require ('../model');
const         { logger }                        = require ('../util/logger');



async function read_apiA (req, res, next) {
        try {
                logger.info ('- /api/file/read read_apiA ()');
        
                let   { mobile }        = req.body;
        
                let     resjosn         = {
                        status: 100,
                        item:   'Hello Express'
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


async function create_apiA (req, res, next) {
        try {
                
                logger.info ('- /api/file/create create_apiA ()');
        
                let   { userid,
                        name,
                        fileid,
                        hash,
                        ext,
                        type,
                        size,
                        uType,
                        // public,
                        width,
                        height }        = req.body;
        
                        
                let   { File }          = models;
                let     user_tmp        = {
                        uid:             userid,
                        name,
                        fileid,
                        hash,
                        ext,
                        type,
                        size,
                        uType,
                        // public,
                        extra: {
                                width,
                                height
                        }
                };
                
                let     file_ro =
                await File.create (user_tmp, { raw: true });
        
                console.log (JSON.stringify (file_ro, null, 4));
        
                let     resjosn         = {
                        status: 100,
                        item:   file_ro
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}

async function upload_apiA (req, res, next) {
        try {
                
                logger.info (req.files);
                let   { username,
                        gender,
                        occupation }    = req.body;
        
        
                let     resjosn         = {
                        status: 100,
                        item:   []
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}

module.exports = {
        read_apiA,
        create_apiA,
        upload_apiA,
};
