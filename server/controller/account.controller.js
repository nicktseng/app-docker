const   models                  = require ('../model');
const   YMError                 = require ('../helper/YMError');
const   ConstHelper             = require ('../helper/consts.helper');
const { logger }                = require ('../util/logger');



async function read_apiA (req, res, next) {
        try {

                logger.info ('- /api/account/read read_apiA ()');
        
                let   { userID }        = req.body;
                // let     headers         = req.headers
                
                let   { User,
                        File,
                        Order }         = models;
                        
                let     user_ro =
                await User.findByPk (userID);
                // await User.findByPk (userID, { include: [
                //         {
                //                 model: File,
                //                 required: false,
                //         },
                //         {
                //                 model: Order,
                //                 required: false,
                //         }
                // ]});
        
                if (!user_ro) {
                        throw new YMError (ConstHelper.kBillingErrorNotFound, 'no match user');
                }
        
                logger.info (`- /api/account/read read_apiA ():: ${JSON.stringify (user_ro,null,4)}`);
        
                let     resjosn         = {
                        status: 100,
                        item:   user_ro
                };
        
                return res.json (resjosn);
        }
        catch (error) {
                next (error);
        }
}


async function create_apiA (req, res, next) {
        try {
                
                logger.info ('- /api/account/create create_apiA ()');
        
                let   { phonenum,
                        username,
                        password,
                        nickname,
                        name,
                        sex,
                        avatar,
                        title,
                        position,
                        province,
                        city,
                        district,
                        street,
                        role }          = req.body;
        
                        
                let   { User }          = models;
                
                let     user_ro =
                await User.create (req.body, { raw: true });
        
                logger.info (JSON.stringify (user_ro, null, 4));
        
                let     resjosn         = {
                        status: 100,
                        item:   user_ro
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


async function update_apiA (req, res, next) {
        try {
                
                logger.info ('- /api/account/update update_apiA ()');
        
                let   { fileID,
                        userID }        = req.body;
        
                        
                let   { User }          = models;
                
                let     user_ro =
                await User.update ({ fk_avatar: fileID }, { where: { id: userID } });
        
                logger.info (JSON.stringify (user_ro, null, 4));
        
                let     resjosn         = {
                        status: 100,
                        item:   user_ro
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


module.exports = {
        read_apiA,
        create_apiA,
        update_apiA,
};
