const           models                          = require ('../model');
const         { logger }                        = require ('../util/logger');



async function readAll_apiA (req, res, next) {
        try {
                
                logger.info ('- /api/order/readAll read_apiA ()');
        
                let   { userID }        = req.body;
        
                let   { User,
                        Order }         = models;
                let     where   = {
                        fk_t_createdBy:   userID,
                };
        
                let     opts    = {
                        include: [
                                {
                                        model: User,
                                        required: true,
                                }
                        ],
                        limit:  10,
                        offset: 0
                };
        
                let     order_ro =
                await Order.findAndCountAll ({ where, ...opts });
        
                // console.log (JSON.stringify (order_ro, null, 4));
        
                let     resjosn         = {
                        status: 100,
                        item:   order_ro
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


async function create_apiA (req, res, next) {
        try {
                
                logger.info ('- /api/order/create create_apiA ()');
        
                let   { channel,
                        orderid,
                        total,
                        status,
                        payType,
                        payStatus,
                        paidAt,
                        deliveredAt,
                        shipping,
                        shippingFee,
                        shippingMethod,
                        comment,
                        userID }       = req.body;
        
                        
                        
                let   { Order }         = models;
                
                let     order_tmp       = {
                        channel,
                        orderid,
                        total:          total*100,
                        status,
                        payType,
                        payStatus,
                        paidAt,
                        deliveredAt,
                        shipping,
                        shippingFee:    shippingFee*100,
                        shippingMethod,
                        comment,
                        fk_t_createdBy: userID,
                };
        
                let     order_ro =
                await Order.create (order_tmp, { raw: true });
        
                // console.log (JSON.stringify (order_ro, null, 4));
        
                let     resjosn         = {
                        status: 100,
                        item:   order_ro
                };
        
                return res.json (resjosn);
        } catch (error) {
                next(error);
        }
}


module.exports = {
        readAll_apiA,
        create_apiA,
};
