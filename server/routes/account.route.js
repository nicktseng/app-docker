const router = require('express').Router();
const AccountController = require('../controller/account.controller');

/**
 * @swagger
 * /api/account/read:
 *  post:
 *    summary: '使用者註冊'
 *    tags: [Account]
 *    requestBody:
 *      $ref: '#/components/requestBodies/Account'
 *    responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200/Account'
 *      400:
 *        $ref: '#/components/responses/400'
 */
router.post('/read', AccountController.read_apiA);

router.post('/create', AccountController.create_apiA);
router.post('/update', AccountController.update_apiA);

module.exports = router;
