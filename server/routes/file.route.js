const   router                  = require ('express').Router ();
const   FileController          = require ('../controller/file.controller');
const   multer                  = require ('multer');

const upload = multer({
        fieldNameSize: 100,
        fieldSize: 1024*1024,
        fields: 20,
        fileSize: 2*1024*1024,
        files: 10,
        parts: 20,

}).any();



router.post ('/read', FileController.read_apiA);
router.post ('/create', FileController.create_apiA);
router.post ('/upload', upload, FileController.upload_apiA);


module.exports = router;
