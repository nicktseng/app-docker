const   router                  = require ('express').Router ();
const   OrderController         = require ('../controller/order.controller');

router.post ('/readAll', OrderController.readAll_apiA);
router.post ('/create', OrderController.create_apiA);


module.exports = router;
