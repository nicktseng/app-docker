# React + TypeScript + Vite

## Docker Build & Run

```bash
# build image tag v1.0.0
docker build -t server:v1.0.0 .

# 指定 docker file
docker build -t server:v1.0.0 -f ./Deploy.Dockerfile .
```

```bash
# container name server_v1.0.0, image server:v1.0.0
docker run --name server_v1.0.0 -p 3090:3090 -d server:v1.0.0
```
