module.exports = (sequelize, DataTypes) => {
        const Item = sequelize.define(
                'Item',
                {
                        // order_id: { type: DataTypes.INTEGER, comment: 'orderID' },
                        price: { type: DataTypes.INTEGER, comment: '價格' },
                },
                {
                        paranoid: true,
                        tableName: 't_items',
                }
        );

        Item.associate = (models) => {
                const { User, Order } = models;

                // * 外鍵字段記錄在 Orders, source belongsTo target
                // * 外鍵字段記錄在 Orders, target hasMany source
                Item.belongsTo(User, { foreignKey: 'fk_t_createdBy' });
                Item.belongsTo(Order, { foreignKey: 'fk_t_order_id' });
        };

        return Item;
};
