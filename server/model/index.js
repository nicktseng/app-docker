const   fs                      = require ('fs');
const   path                    = require ('path');
const   Sequelize               = require ('sequelize');
const   config                  = require ('config');
const { logger }                = require ('../util/logger');

const   basename                = path.basename (__filename);
const   db                      = {};
const   seqCfg                  = config.sequelize;

const   sequelize       = new Sequelize(seqCfg.database, seqCfg.username, seqCfg.password, seqCfg.options);

fs.readdirSync(__dirname)
        .filter(
                (file) =>
                        file.indexOf('.') !== 0 &&
                        file !== basename &&
                        file.slice(-3) === '.js' &&
                        file.indexOf('.test.js') === -1
        )
        .forEach((file) => {
                const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);

                db[model.name] = model;
        });

Object.keys(db).forEach((modelName) => {
        if (db[modelName].associate) {
                db[modelName].associate(db);
        }
});

function initDB () {

        sequelize.authenticate()
                .then(() => {
                        logger.info (`DB_NAME:${seqCfg.database} Connection has been established successfully.`);
                })
                .catch((err) => {
                        logger.error(`Unable to connect to the database DB_NAME:${seqCfg.database}`, err);
                });
        
        // * 只做一次資料庫 alter sync 用作外鍵約束，關聯，複合索引
        sequelize.sync({
                // alter: true,
                // force: true
        });
}

function closeDB () {
        sequelize.connectionManager.close()
                .then(() => {
                        logger.info ('DB disconnect successfully.');
                })
                .catch (() => {
                        logger.info ('DB disconnect fail.');
                });
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.initDB = initDB;
db.closeDB = closeDB;

module.exports = db;
