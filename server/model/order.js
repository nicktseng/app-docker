/* 客户订单 */
/*
  淘宝订单状态: https://open.taobao.com/doc.htm?docId=102856&docType=1
  status: -1:交易关闭, 0:待支付, 1:待审核, 2:待发货, 3:已发货, 4:已签收, 5.交易成功,
  10:退货审核中, 11:审核失败, 12:审核成功, 13:待退款, 14:退款成功,
  payType: 1:微信支付, 2:支付宝
  payStatus: 0:待支付, 1:已支付, 2:待退款, 3:已退款,
  shippingMethod: 0:快递, 1:当面取货
*/
module.exports = (sequelize, DataTypes) => {
        const Order = sequelize.define(
                'Order',
                {
                        channel: { type: DataTypes.INTEGER, comment: '售卖渠道', defaultValue: 0 },
                        orderid: { type: DataTypes.STRING, comment: '订单编号', allowNull: false },
                        total: {
                                type: DataTypes.INTEGER,
                                comment: '总金额',
                                defaultValue: 0,
                                get() {
                                        return this.getDataValue('total') / 100;
                                },
                                set(val) {
                                        console.log(`- order total::${val}`);
                                        this.setDataValue('total', Math.round(val * 100));
                                },
                        },
                        status: { type: DataTypes.INTEGER, comment: '订单状态', defaultValue: 0 },

                        payType: { type: DataTypes.INTEGER, comment: '支付类型' },
                        payStatus: {
                                type: DataTypes.INTEGER,
                                comment: '支付状态',
                                defaultValue: 0,
                        },
                        paidAt: { type: DataTypes.DATE, comment: '支付时间' },

                        deliveredAt: { type: DataTypes.DATE, comment: '发货时间' },
                        // express: { type: DataTypes.STRING, comment: '快递单编号' },

                        shipping: { type: DataTypes.JSON, comment: '收货地址' },
                        shippingFee: {
                                type: DataTypes.INTEGER,
                                comment: '快递费',
                                defaultValue: 0,
                                get() {
                                        return this.getDataValue('shippingFee') / 100;
                                },
                                set(val) {
                                        this.setDataValue('shippingFee', Math.round(val * 100));
                                },
                        },
                        shippingMethod: {
                                type: DataTypes.INTEGER,
                                comment: '取货方式',
                                defaultValue: 0,
                        },

                        comment: { type: DataTypes.STRING, comment: '订单备注' },
                },
                {
                        paranoid: true,
                        tableName: 't_orders',
                }
        );

        Order.associate = (models) => {
                const { User, Address } = models;

                // * 外鍵字段記錄在 Orders, source belongsTo target
                // * 外鍵字段記錄在 Orders, target hasMany source
                Order.belongsTo(Address, { foreignKey: 'fk_t_address' });
                Order.belongsTo(User, { foreignKey: 'fk_t_createdBy', constraints: false });
                User.hasMany(Order, { foreignKey: 'fk_t_createdBy' });
        };

        return Order;
};
