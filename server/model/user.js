// const createError = require('http-errors');
// const bcrypt = require('bcrypt');
// const   logger                  = require ('../util/logger');

// role: 1:销售代表,999:超级用户
module.exports = (sequelize, DataTypes) => {
        const User = sequelize.define(
                'User',
                {
                        username: { type: DataTypes.STRING, comment: '姓名' },
                        password: { type: DataTypes.STRING, comment: '密碼' },
                        nickname: { type: DataTypes.STRING, comment: '昵称' },
                        name: { type: DataTypes.STRING, comment: '真實姓名' },
                        birthday: { type: DataTypes.DATE, comment: '生日' },
                        email: {
                                type: DataTypes.STRING,
                                comment: '郵件',
                                unique: true,
                                index: true,
                        },
                        phonenum: { type: DataTypes.STRING, comment: '手機號碼', unique: true },
                        sex: { type: DataTypes.INTEGER, comment: '性别' },
                        // avatar: { type: DataTypes.STRING, comment: '頭像' }, // { id, url }
                        title: { type: DataTypes.STRING, comment: '職稱' },
                        position: { type: DataTypes.STRING, comment: '職務' },
                        province: { type: DataTypes.INTEGER, comment: '所在省份' },
                        city: { type: DataTypes.INTEGER, comment: '所在城市' },
                        district: { type: DataTypes.INTEGER, comment: '所在縣區' },
                        street: { type: DataTypes.STRING, comment: '地址' },
                        role: { type: DataTypes.INTEGER, comment: '角色', defaultValue: 1 },
                },
                {
                        paranoid: true,
                        tableName: 't_users',
                        //     instanceMethods: {
                        //       async validPassword(password) {
                        //         return bcrypt.compare(password, this.password);
                        //       },
                        //     },
                        //     hooks: {
                        //       async beforeCreate(user, options) {
                        //         log.trace({ user, options }, 'beforeCreate');

                        //         if (user.username) {
                        //           const count = await User.count({
                        //             where: { username: user.username }, transaction: options.transaction,
                        //           });

                        //           if (count > 0) throw createError(404, '用户名已存在');
                        //         }

                        //         if (!user.password) return user;

                        //         user.set('password', await bcrypt.hash(user.password, 10));

                        //         return user;
                        //       },

                        //       async beforeUpdate(user, options) {
                        //         log.trace({ user, options }, 'beforeUpdate');

                        //         if (user.changed('username')) {
                        //           const count = await User.count({
                        //             where: { username: user.username }, transaction: options.transaction,
                        //           });

                        //           if (count > 0) throw createError(404, '用户名已存在');
                        //         }

                        //         if (user.changed('password')) {
                        //           user.set('password', await bcrypt.hash(user.password, 10));
                        //         }

                        //         return user;
                        //       },
                        //     },
                }
        );

        User.associate = (models) => {
                const { File } = models;

                User.belongsTo(File, {
                        foreignKey: 'fk_t_avatar',
                        as: 'avatar',
                        constraints: false,
                        comment: '頭像',
                });

                User.belongsTo(User, {
                        foreignKey: 'fk_t_referee',
                        as: 'referee',
                        comment: '推薦人',
                });
        };

        return User;
};
