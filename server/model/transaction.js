// status: 0: 'waitPay', 1: 'success', 2: 'fail', -1: 'closed',
// type: 1:微信支付, 2:支付宝

module.exports = (sequelize, DataTypes) => {
        const Transaction = sequelize.define(
                'Transaction',
                {
                        number: { type: DataTypes.STRING, comment: '交易流水號' },
                        order: { type: DataTypes.STRING, comment: '訂單號' },
                        subject: { type: DataTypes.STRING, comment: '商品標題' },
                        amount: { type: DataTypes.INTEGER, comment: '金額(貨幣最小單位)' },
                        type: { type: DataTypes.STRING, comment: '支付類型' },
                        status: { type: DataTypes.INTEGER, comment: '交易狀態', defaultValue: 0 },
                        payment: { type: DataTypes.JSON, comment: '交易訊息' },
                },
                {
                        paranoid: true,
                        tableName: 't_transactions',
                }
        );

        return Transaction;
};
