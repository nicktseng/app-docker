// type: 1:image,2:video
const config = require('config');

module.exports = (sequelize, DataTypes) =>
        sequelize.define(
                'File',
                {
                        name: { type: DataTypes.STRING, comment: '文件名' },
                        fileid: { type: DataTypes.STRING, comment: '文件ID' },
                        hash: { type: DataTypes.STRING, comment: '文件etag' },
                        ext: { type: DataTypes.STRING, comment: '文件附檔名' },
                        type: { type: DataTypes.INTEGER, comment: '文件類型', enum: [1, 2] },
                        size: { type: DataTypes.INTEGER, comment: '文件大小' },
                        uType: { type: DataTypes.INTEGER, comment: '用户類型', enum: [1, 2] },
                        userid: { type: DataTypes.INTEGER },
                        public: {
                                type: DataTypes.BOOLEAN,
                                comment: '公開訪問',
                                defaultValue: true,
                        },
                        extra: {
                                type: DataTypes.JSON,
                                comment: '其他信息',
                                schema: {
                                        width: DataTypes.INTEGER,
                                        height: DataTypes.INTEGER,
                                },
                        },
                },
                {
                        paranoid: true,
                        tableName: 't_files',
                        getterMethods: {
                                url() {
                                        const key = this.getDataValue('key');

                                        return `${config.qiniu.host}/${key}`;
                                },
                        },
                }
        );
