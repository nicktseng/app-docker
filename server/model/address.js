/* 客户/销售代表收货地址 */
module.exports = (sequelize, DataTypes) => {
        const Address = sequelize.define(
                'Address',
                {
                        uType: { type: DataTypes.INTEGER, comment: '用户類型', defaultValue: 1 },
                        uid: { type: DataTypes.INTEGER, comment: '銷售代表/客户ID' },
                        name: { type: DataTypes.STRING, comment: '真實姓名' },
                        province: { type: DataTypes.STRING, comment: '省' },
                        city: { type: DataTypes.STRING, comment: '市' },
                        district: { type: DataTypes.STRING, comment: '縣區' },
                        zipcode: { type: DataTypes.STRING, comment: '郵遞區' },
                        street: { type: DataTypes.STRING, comment: '地址' },
                        phone: { type: DataTypes.STRING, comment: '市話號碼' },
                        mobile: { type: DataTypes.STRING, comment: '手機號碼' },
                        isDefault: {
                                type: DataTypes.BOOLEAN,
                                comment: '默認地址',
                                defaultValue: false,
                        },
                },
                {
                        paranoid: true,
                        tableName: 't_address',
                }
        );

        return Address;
};
