const requestBodies = {
        Account: {
                required: true,
                content: {
                        'application/json': {
                                schema: {
                                        type: 'object',
                                        required: ['userID'],
                                        properties: {
                                                userID: {
                                                        type: 'string',
                                                        description: '主鍵',
                                                },
                                        },
                                        example: {
                                                userID: 2,
                                        },
                                },
                        },
                },
        },
        Signup: {
                required: true,
                content: {
                        'application/json': {
                                schema: {
                                        type: 'object',
                                        required: ['name', 'email', 'password', 'confirmPassword'],
                                        properties: {
                                                name: {
                                                        type: 'string',
                                                        description: '使用者的名稱',
                                                },
                                                email: {
                                                        type: 'string',
                                                        description: '使用者的信箱 (作為登入帳號)',
                                                },
                                                password: {
                                                        type: 'string',
                                                        description: '使用者的密碼 (作為登入密碼)',
                                                },
                                                confirmPassword: {
                                                        type: 'string',
                                                        description: '確認密碼',
                                                },
                                                roleId: {
                                                        type: 'integer',
                                                        description:
                                                                '使用者的角色 (權限) ID，非必填、預設為 general',
                                                },
                                        },
                                },
                        },
                },
        },
};

module.exports = requestBodies;
