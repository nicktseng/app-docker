const responses = {
        400: {
                description: '請求中的值無效或格式錯誤',
                contenets: 'application/json',
        },
        401: {
                description: 'Unauthorized - token 的值無效或格式錯誤',
                contenets: 'application/json',
        },
        200: {
                Account: {
                        type: 'object',
                        description: 'Account 資訊',
                        properties: {
                                id: {
                                        type: 'integer',
                                        description: 'ID',
                                },
                                role: {
                                        type: 'integer',
                                        description: '角色 (權限)',
                                },
                                username: {
                                        type: 'string',
                                        description: '姓名',
                                },
                                password: {
                                        type: 'string',
                                        description: '密碼',
                                },
                                email: {
                                        type: 'string',
                                        description: '郵箱',
                                },
                                phonenum: {
                                        type: 'string',
                                        description: '手機號碼',
                                },
                                sex: {
                                        type: 'integer',
                                        description: '性別',
                                },
                                country: {
                                        type: 'string',
                                        description: '國家',
                                },
                                district: {
                                        type: 'integer',
                                        description: '行政區',
                                },
                                city: {
                                        type: 'integer',
                                        description: '縣市',
                                },
                                birthday: {
                                        type: 'string',
                                        description: '生日',
                                },
                                title: {
                                        type: 'string',
                                        description: '職稱',
                                },
                                position: {
                                        type: 'string',
                                        description: '職位',
                                },
                                street: {
                                        type: 'string',
                                        description: '詳細地址',
                                },
                                province: {
                                        type: 'integer',
                                        description: '省份',
                                },
                                updatedAt: {
                                        type: 'string',
                                        description: '更新日期',
                                },
                                createdAt: {
                                        type: 'string',
                                        description: '創建日期',
                                },
                        },
                        example: {
                                id: 2,
                                role: 1,
                                username: 'tim',
                                password: 'abc123',
                                email: 'tim@gmail.com',
                                phonenum: '0988525252',
                                sex: 1,
                                country: 'TW',
                                district: 3000,
                                city: 4010,
                                birthday: '1992-11-09T13:25:32.000Z',
                                title: 'Project Engineer',
                                position: 'pm',
                                street: 'Zhong Zheng road',
                                province: 5020,
                                updatedAt: '2023-12-05T03:41:36.340Z',
                                createdAt: '2023-12-05T03:41:36.340Z',
                        },
                },
        },
};

module.exports = responses;
