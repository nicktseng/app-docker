const   util            = require ('util');

exports.kBillingSuccess                         = 100;

exports.kBillingErrorGeneric                    = 500;
exports.kBillingErrorInvalidParam               = 501;
exports.kBillingErrorCheckSigError              = 502;
exports.kBillingErrorInvalidConfig              = 503;
exports.kBillingErrorInvalidContentType         = 504;
exports.kBillingErrorNotFound                   = 505;
exports.kBillingErrorNoAvailableResource        = 506;
exports.kBillingErrorNetworkError               = 507;
exports.kBillingErrorInvalidFormat              = 508;
exports.kBillingErrorInvalidOperation           = 509;
exports.kBillingErrorFrequentRequest            = 510;

exports.kBillingErrorNotLoggedIn                = 520;
exports.kBillingErrorPermissionDenied           = 521;
exports.kBillingErrorAccess                     = 522;
exports.kBillingErrorDuplicateInfos             = 523;
exports.kBillingErrorPaymentFail                = 524;
exports.kBillingErrorInvoiceFail                = 525;

exports.kBillingErrorHaveNotCompletedOrder      = 530;

exports.kBillingErrorSesionExpired              = 540;




exports.throw = function (statusCode, msg) {
        let err         = new Error (msg);
        err.status      = statusCode;
        throw err;
};

function YMError (status, message, data) {
        Error.captureStackTrace (this, this.constructor);        // v8-specific
        this.status     = status;
        this.message    = message;
        this.data       = data;
}

util.inherits (YMError, Error);

exports.YMError         = YMError;

// throw new Consts.YMError (Consts.kBillingSuccess, '成功')