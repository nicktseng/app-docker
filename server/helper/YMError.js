
// * https://www.youtube.com/watch?v=BZPrK1nQcFI
class YMError extends Error {

        constructor (statusCode, message) {
                super(statusCode, message);

                this.statusCode = statusCode;
                this.message = message;
                this.status = statusCode >= 400 && statusCode < 500 ? 'fail' : 'error';

                this.isOperational = true;

                Error.captureStackTrace (this, this.constructor);        // v8-specific
        }
}

module.exports = YMError;

// throw new ConstHelper.YMError (ConstHelper.kBillingErrorNotFound, errMsg);
