'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('t_orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      channel: { type: Sequelize.INTEGER, comment: '售卖渠道', defaultValue: 0 },
      orderid: { type: Sequelize.STRING, comment: '订单编号', allowNull: false },
      total: {
        type: Sequelize.INTEGER,
        comment: '总金额',
        defaultValue: 0,
        get() {
          return this.getDataValue('total') / 100;
        },
        set(val) {
          console.log (`- order total::${val}`);
          this.setDataValue('total', Math.round(val * 100));
        },
      },
      status: { type: Sequelize.INTEGER, comment: '订单状态', defaultValue: 0 },
      payType: { type: Sequelize.INTEGER, comment: '支付类型' },
      payStatus: { type: Sequelize.INTEGER, comment: '支付状态', defaultValue: 0 },
      paidAt: { type: Sequelize.DATE, comment: '支付时间' },
      deliveredAt: { type: Sequelize.DATE, comment: '发货时间' },
      // express: { type: Sequelize.STRING, comment: '快递单编号' },

      shipping: { type: Sequelize.JSON, comment: '收货地址' },
      shippingFee: {
        type: Sequelize.INTEGER,
        comment: '快递费',
        defaultValue: 0,
        get() {
          return this.getDataValue('shippingFee') / 100;
        },
        set(val) {
          this.setDataValue('shippingFee', Math.round(val * 100));
        },
      },
      shippingMethod: { type: Sequelize.INTEGER, comment: '取货方式', defaultValue: 0 },

      comment: { type: Sequelize.STRING, comment: '订单备注' },

      fk_t_createdBy: {
        type: Sequelize.INTEGER
      },
      fk_t_address: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('t_orders');
  }
};