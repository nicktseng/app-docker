'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('t_users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: { type: Sequelize.STRING, comment: '姓名' },
      password: { type: Sequelize.STRING, comment: '密碼' },
      nickname: { type: Sequelize.STRING, comment: '昵称' },
      name: { type: Sequelize.STRING, comment: '真實姓名' },
      birthday: { type: Sequelize.DATE, comment: '生日' },
      email: { type: Sequelize.STRING, comment: '郵件', allowNull: false },
      phonenum: { type: Sequelize.STRING, comment: '手機號碼', allowNull: false },
      sex: { type: Sequelize.INTEGER, comment: '性别' },
      title: { type: Sequelize.STRING, comment: '職稱' },
      position: { type: Sequelize.STRING, comment: '職務' },
      province: { type: Sequelize.INTEGER, comment: '所在省份' },
      city: { type: Sequelize.INTEGER, comment: '所在城市' },
      district: { type: Sequelize.INTEGER, comment: '所在縣區' },
      street: { type: Sequelize.STRING, comment: '地址' },
      role: { type: Sequelize.INTEGER, comment: '角色', defaultValue: 1 },
      fk_t_referee: {
        type: Sequelize.INTEGER
      },
      fk_t_avatar: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('t_users');
  }
};