'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('t_transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      number: { type: Sequelize.STRING, comment: '交易流水號' },
      order: { type: Sequelize.STRING, comment: '訂單號' },
      subject: { type: Sequelize.STRING, comment: '商品標題' },
      amount: { type: Sequelize.INTEGER, comment: '金額(貨幣最小單位)' },
      type: { type: Sequelize.STRING, comment: '支付類型' },
      status: { type: Sequelize.INTEGER, comment: '交易狀態', defaultValue: 0 },
      payment: { type: Sequelize.JSON, comment: '交易訊息' },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('t_transactions');
  }
};