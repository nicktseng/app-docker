'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('t_address', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uType: { type: Sequelize.INTEGER, comment: '用户類型', defaultValue: 1 },
      uid: { type: Sequelize.INTEGER, comment: '銷售代表/客户ID' },
      name: { type: Sequelize.STRING, comment: '真實姓名' },
      province: { type: Sequelize.STRING, comment: '省' },
      city: { type: Sequelize.STRING, comment: '市' },
      district: { type: Sequelize.STRING, comment: '縣區' },
      zipcode: { type: Sequelize.STRING, comment: '郵遞區' },
      street: { type: Sequelize.STRING, comment: '地址' },
      phone: { type: Sequelize.STRING, comment: '市話號碼' },
      mobile: { type: Sequelize.STRING, comment: '手機號碼' },
      isDefault: { type: Sequelize.BOOLEAN, comment: '默認地址', defaultValue: false },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('t_address');
  }
};