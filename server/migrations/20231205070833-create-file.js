'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('t_files', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING, comment: '文件名'
      },
      fileid: {
        type: Sequelize.STRING, comment: '文件ID'
      },
      hash: {
        type: Sequelize.STRING, comment: '文件etag'
      },
      ext: {
        type: Sequelize.STRING, comment: '文件附檔名'
      },
      type: {
        type: Sequelize.INTEGER, comment: '文件類型', enum: [1, 2]
      },
      size: {
        type: Sequelize.INTEGER, comment: '文件大小'
      },
      uType: {
        type: Sequelize.INTEGER, comment: '用户類型', enum: [1, 2]
      },
      userid: {
        type: Sequelize.INTEGER
      },
      public: {
        type: Sequelize.BOOLEAN,
        comment: '公開訪問',
        defaultValue: true
      },
      extra: {
        type: Sequelize.JSON,
        comment: '其他信息',
        schema: {
          width: Sequelize.INTEGER,
          height: Sequelize.INTEGER,
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('t_files');
  }
};