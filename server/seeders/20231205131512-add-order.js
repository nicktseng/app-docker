'use strict';

/** @type {import('sequelize-cli').Migration} */
const { faker } = require('@faker-js/faker');
module.exports = {
        async up(queryInterface, Sequelize) {
                let dummyJSON = [];
                let arr = Array.from(Array(100), (v, k) => k);
                arr.map(() => {
                        dummyJSON.push({
                                orderid: faker.string.alphanumeric(10),
                                total: faker.number.int({ min: 100, max: 5000 }),
                                fk_t_createdBy: faker.number.int({ min: 1, max: 100 }),
                                createdAt: new Date(),
                                updatedAt: new Date(),
                        });
                });

                await queryInterface.bulkInsert('t_orders', dummyJSON, {});
        },

        async down(queryInterface, Sequelize) {
                await queryInterface.bulkDelete('t_orders', null, {});
        },
};
