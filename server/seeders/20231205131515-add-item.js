'use strict';

/** @type {import('sequelize-cli').Migration} */
const { faker } = require('@faker-js/faker');
module.exports = {
        async up(queryInterface, Sequelize) {
                let dummyJSON = [];
                let arr = Array.from(Array(100), (v, k) => k);
                arr.map(() => {
                        dummyJSON.push({
                                price: faker.number.int({ min: 100, max: 1000 }),
                                fk_t_order_id: faker.number.int({ min: 1, max: 100 }),
                                createdAt: new Date(),
                                updatedAt: new Date(),
                        });
                });

                await queryInterface.bulkInsert('t_items', dummyJSON, {});
        },

        async down(queryInterface, Sequelize) {
                await queryInterface.bulkDelete('t_items', null, {});
        },
};
