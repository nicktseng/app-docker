'use strict';

/** @type {import('sequelize-cli').Migration} */
const { faker } = require('@faker-js/faker');
module.exports = {
        async up(queryInterface, Sequelize) {
                let dummyJSON = [];
                let arr = Array.from(Array(100), (v, k) => k);
                arr.map(() => {
                        dummyJSON.push({
                                name: faker.internet.userName(),
                                email: faker.internet.email(),
                                phonenum: faker.phone.number(),
                                position:
                                        faker.number.int({ min: 1, max: 10 }) > 5
                                                ? 'maneger'
                                                : 'engineer',
                                createdAt: new Date(),
                                updatedAt: new Date(),
                        });
                });

                await queryInterface.bulkInsert('t_users', dummyJSON, {});
        },

        async down(queryInterface, Sequelize) {
                await queryInterface.bulkDelete('t_users', null, {});
        },
};
