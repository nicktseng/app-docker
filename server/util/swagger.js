const   path                    = require ("path");
const   express                 = require ("express");
const   swaggerUI               = require ("swagger-ui-express");
const   swaggerDoc              = require ("swagger-jsdoc");
const   schemas                 = require ('../swagger-schema/schema');
const   responses               = require ('../swagger-schema/response');
const   requestBodies           = require ('../swagger-schema/requestBodies');


// *配置swagger-jsdoc
const   options = {
        definition: {
                openapi:                "3.0.0",
                info: {
                        title:          "API 文件",
                        version:        "1.0.0",
                        description:    `本伺服器使用 Express 搭建，作為「企業官網後台管理和前台撈取資料用」的後端，相應的 API 分別是 admin 和 web 兩部份。以下 API 文件 ，均以資料 model 作為分類名稱並列出所有路由訊息，如：Users、News 等等，其中以 web 開頭的名稱供前台使用，如：WebNews、WebProducts 等等，無須 token 驗證；反之，非以 web 開頭的分類即是供給後台使用，後台路由除了 login 和 signup 皆須 token 驗證。`,
                },
                components: {
                        securitySchemes: {
                                bearerAuth: {
                                        type:           'http',
                                        in:             'header',
                                        name:           'Authorization',
                                        description:    'Bearer Token',
                                        scheme:         'bearer',
                                        bearerFormat:   'JWT',
                                },
                        },
                        schemas,
                        responses,
                        requestBodies,
                },
                security: [{ bearerAuth: [] }],
                servers: [
                        {
                                url: 'http://owsb.ap-northeast-1.elasticbeanstalk.com',
                                description: 'AWS server',
                        },
                        {       url: 'http://localhost:3090',
                                description: 'Local server'
                        },
                ],
        },
        // * 去哪個路由下收集 swagger 注釋
        apis: [path.join (__dirname, "../routes/*.js")],
};

const   swaggerSpec = swaggerDoc (options);

function swaggerJson (req, res) {
        res.setHeader ("Content-Type", "application/json");
        res.send (swaggerSpec);
}


function swaggerInstall (app) {
        if (!app) {
                app = express();
        }
        // * 開放相關接口，
        app.get ("/swagger.json", swaggerJson);
        // * 使用 swaggerSpec 生成 swagger 文檔頁面，並開放在指定路由
        app.use ("/swagger", swaggerUI.serve, swaggerUI.setup (swaggerSpec));
}

module.exports = swaggerInstall;
