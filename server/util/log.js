const bunyan = require('bunyan');
const config = require('config');

const log = bunyan.createLogger({
        name: config.app.title,
        level: process.env.NODE_LOG_LEVEL || 'info',
});

module.exports = log;
