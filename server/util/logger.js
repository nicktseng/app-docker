const { createLogger, format, transports, addColors } = require('winston');
const LOG_LEVEL = 'debug';

class WinstonLogger {
        constructor(level) {
                const config = {
                        levels: {
                                error: 0,
                                warn: 1,
                                info: 2,
                                verbose: 3,
                                debug: 4,
                                silly: 5,
                        },
                        colors: {
                                error: 'red',
                                warn: 'red',
                                info: 'green',
                                verbose: 'cyan',
                                debug: 'blue',
                                silly: 'magenta',
                        },
                };

                const formatParams = (info) => {
                        let { timestamp, level, message } = info;
                        // message = message.toString().replace(/[\r\n]/g, "");
                        return `[${timestamp}] ${level}: ${message}`;
                };

                addColors(config.colors);
                this.logger = createLogger({
                        // filename: 'application-%DATE%.log',
                        level,
                        levels: config.levels,
                        handleExceptions: true,
                        format: format.combine(
                                format.colorize(),
                                format.timestamp({ format: 'YYYY-MM-DD hh:mm:ss.SSS A' }),
                                format.printf(formatParams)
                        ),
                        transports: [new transports.Console()],
                });
        }

        debug(msg) {
                this.logger.debug(msg);
        }

        info(msg) {
                this.logger.info(msg);
        }

        warn(msg) {
                this.logger.warn(msg);
        }

        error(msg) {
                this.logger.error(msg);
        }
}

const logger = new WinstonLogger(LOG_LEVEL);

module.exports = {
        logger,
};
