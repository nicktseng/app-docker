module.exports = {
        app: { title: '用戶管理系統', port: 3090 },
        env: process.env.NODE_ENV,
        host: 'http://127.0.0.1:3090',
        sequelize: {
                dialect: 'mysql',
                operatorsAliases: false,
                logging: (sql, options) => {
                        // let logger = require('../util/logger'); // eslint-disable-line global-require
                        // logger.info({ sql });
                        // logger.debug({ options });
                },
                options: {
                        host: process.env.MYSQL_HOST || '127.0.0.1',
                        port: process.env.MYSQL_PORT || 3306,
                        dialect: 'mysql',
                        operatorsAliases: '0',
                        define: {
                                freezeTableName: true,
                        },
                        logging: (sql, options) => {
                                // let logger = require('../util/logger'); // eslint-disable-line global-require
                                // logger.info({ sql });
                                // logger.debug({ options });
                        },
                        pool: {
                                max: 5,
                                min: 0,
                                acquire: 30000,
                                idle: 10000,
                        },
                },
                database: 'db_dev',
                username: 'root',
                password: '1234',
        },
        express: {
                view: {
                        path: './views',
                },
                cors: {
                        origin: true,
                        credentials: true,
                        exposedHeaders: 'Total',
                },
        },
        servers: {},
        sms: { debug: false, yunpian: {}, '7moor': {} },
        bodyParser: { json: { strict: false, limit: '5mb' } },
        jwt: { secret: '?', expiresIn: '30d' },
        log: { format: 'combined' },
};
