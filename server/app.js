const   express                 = require ('express');
const   bodyParser              = require ('body-parser');
const   favicon                 = require ('serve-favicon');
const   compress                = require ('compression');
const   _                       = require ('lodash');
const   helmet                  = require ('helmet');
const   path                    = require ('path');
const   config                  = require ('config');
const   cors                    = require ('cors');
const { logger }                = require ('./util/logger');
const   ConstHelper             = require ('./helper/consts.helper');
const   YMError                 = require ('./helper/YMError');
const   db                      = require ('./model');

const   swaggerInstall          = require ("./util/swagger");

const   AccountRoute            = require ('./routes/account.route');
const   FileRoute               = require ('./routes/file.route');
const   OrderRoute              = require ('./routes/order.route');


const app = express ();

swaggerInstall (app);

db.initDB ();

app.use (favicon (__dirname + '/public/images/favicon.ico'));
app.use (helmet.frameguard (_.get(config, 'helmet.frameguard')));
app.use (helmet.xssFilter (_.get(config, 'helmet.xssFilter')));
app.use (helmet.noSniff ());
app.use (helmet.ieNoOpen ());
app.use (helmet.hsts (_.get(config, 'helmet.hsts')));
app.use (helmet.hidePoweredBy ());

app.use (compress ({
        filter (req, res) {
          return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'));
        },
        level: 9,
        ..._.get (config, 'compression.options'),
}));

app.use (bodyParser.urlencoded({
        extended: true, ..._.get(config, 'bodyParser.urlencoded'),
}));
  
app.use (bodyParser.json(_.get(config, 'bodyParser.json')));  
app.options(cors(config.express.cors));
app.use (cors(config.express.cors));
app.use (express.static(path.resolve('public')));
app.use (require("morgan")("dev", { "stream": logger.stream }));

// * Route
app.use ('/api/account', AccountRoute);
app.use ('/api/file', FileRoute);
app.use ('/api/order', OrderRoute);

app.use ('/api/health', (req, res, next) => {

        res.json ({ status: 100, message: 'ok', item: null });
});

app.use ('/api/db/close', (req, res, next) => {

        db.closeDB ();

        res.json ({ status: 100, message: 'ok', item: null });
});

// * No match route handler
app.use (async (req, res, next) => {
        // const user = await User.findByToken(req.get('authorization'));
        // log.warn ('No match route');
        // * pass to error handler
        throw new YMError (ConstHelper.kBillingErrorNotFound, `no match ${req.originalUrl}`);
});

// * Error handler
app.use ((err, req, res, next) => {
        logger.info (err.status);
        let   { status,
                message }       = err;

        if (!status) {
                status          = ConstHelper.kBillingErrorGeneric;
        }

        logger.info (`status: ${status}, ${message}`);
        
        res.json ({ status, message, item: null });
});

module.exports = app;